document.addEventListener('DOMContentLoaded', function(){
	'use strict';

	$('.js-burger').click( function () {
		$(this).toggleClass('is-open');
		$(this).toggleClass('fixed-center');
		$('.js-nav').toggleClass('active');
		$('.header').toggleClass('index');
	});

	$('.nav__item').click( function () {
		$('.js-burger').removeClass('is-open');
		$('.js-burger').removeClass('fixed-center');
		$('.js-nav').removeClass('active');
		$('.header').removeClass('index');

		const section = $(this).data('menuanchor');
		if ($.fn.fullpage.moveTo && $(window).width() > 768) {
			$.fn.fullpage.moveTo(section);
		} else {
			var body = $("html, body");
			const top = $(`.section[data-section="section${section}"]`).position().top - 30;
			body.stop().animate({scrollTop: top}, 500, 'swing', function() { 
			});
		}
	});

});
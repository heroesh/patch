$(document).ready(function () {

	$('.js-slider').slick({
		slidesToShow: 2,
		slidesToScroll: 1,
		pauseOnHover: true,
		dots: false,
		arrows: false,
		infinite: true,
	});
	
});
//=require ../blocks/**/*.js

document.addEventListener('DOMContentLoaded', function(){
	'use strict';
	
	//video
	$('.js-video-open').click( function () {
		$('.js-video').fadeIn();
		$('.js-mask').fadeIn(1000);
	});

	$('.js-video-close').click( function () {
		$('.js-video').fadeOut();
		$('.js-mask').fadeOut();
		var videoURL = $('#video').prop('src');
		videoURL = videoURL.replace("&autoplay=1", "");
		$('#video').prop('src','');
		$('#video').prop('src',videoURL);
	});

	//footer
	$('.js-footer-open').click( function () {
		$('.js-footer').addClass('active');
	});

	$('.js-footer-close').click( function () {
		$('.js-footer').removeClass('active');
	});

	//fullpage
	if ($(window).width() > 768) {
		$("#fullpage").fullpage({
			// anchors:['1page','2page','3page','4page','5page','6page','7page','8page'],
			// menu:'#menu',
			scrollBar:false,
			navigation:true,
			slidesNavigation:true,
			navigationPosition:'left',
			loopTop:true,
			loopBottom:true,
			loopHorizontal:false,
			responsive: 768,
			//events
			onLeave: function(origin, destination, direction){
				$(`.nav__item`).removeClass('active');
				$(`.nav__item[data-menuanchor=${destination}]`).addClass('active');
			}
		});
	}

	// window.fullpage_api = fullpage_api;

	//animation

	// $(function(){
	// 	var OFFSET = 85;
	// 	var WAIT = 10;
	
	// 	// Check the initial Position of the fixed_nav_container
	// 	var stickyHeaderTop0 = $('.fixed_heading_shop').offset().top;
	// 	var isFixed = false; // assuming that's the right default value
	
	// 	$(window).scroll(_.throttle(function(){
	// 	  if($(window).scrollTop() > stickyHeaderTop0 - OFFSET) {
	// 		if(!isFixed) {
	// 		  $('#fixed_heading_shop').css({position: 'fixed', top: OFFSET+'px'});
	// 		  $('#ghost_div0').css({display: 'block'});
	// 		  isFixed = true;
	// 		}
	// 	  }
	// 	  else {
	// 		if(isFixed) {
	// 		  $('#fixed_heading_shop').css({position: 'relative', top: '0px'});
	// 		  $('#ghost_div0').css({display: 'none'});
	// 		  isFixed = false;
	// 		}
	// 	  }
	// 	}, WAIT));
	//   });
	
	// $(window).scroll(function () {
	// 	$('.mov').each(function () {
	// 		var imagePos = $(this).offset().top;
	// 		var topOfWindow = $(window).scrollTop();

	// 		if (imagePos < topOfWindow + 400) {
	// 			$(this).addClass('anim');
	// 		}
	// 	});
	// });

});